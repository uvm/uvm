/*
 * Copyright 2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define _GNU_SOURCE 1

#include "config.h"

#include "uvm.h"

#include <elf.h>

#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

/* FAKE! */
#define UVM_ARCH 0xFFU
/* FAKE! */
#define UVM_ABI 0xFFU

#define MAXIMUM_PHSIZE 1024U

struct segment {
    void *addr;
    size_t size;
};

static uint64_t from_le_64(uint64_t x);
static uint32_t from_le_32(uint32_t x);
static uint16_t from_le_16(uint16_t x);

int main(int argc, char **argv)
{
    char *copy = strdup(argv[0]);
    if (0 == copy) {
        perror("strdup");
        return EXIT_FAILURE;
    }
    char *base_name = basename(copy);
    char const *program_name = strdup(base_name);
    if (0 == program_name) {
        perror("strdup");
        return EXIT_FAILURE;
    }
    free(copy);
    copy = 0;
    base_name = 0;

    if (argc > 2) {
        fprintf(stderr, "%s: too many arguments\n", program_name);
        return EXIT_FAILURE;
    }
    if (argc < 2) {
        fprintf(stderr, "%s: too little arguments\n", program_name);
        return EXIT_FAILURE;
    }

    FILE *f = fopen(argv[1], "r");
    if (NULL == f) {
        perror("fopen");
        return EXIT_FAILURE;
    }

    Elf64_Ehdr header = {0};
    {
        Elf64_Ehdr xx = {0};
        size_t bytes = fread(&xx, 1, sizeof xx, f);
        if (bytes < sizeof xx) {
            if (ferror(f)) {
                perror("fread");
            } else {
                fprintf(stderr, "%s: short file: %zu\n", program_name, bytes);
            }
            return EXIT_FAILURE;
        }
        header = xx;
    }
    if (header.e_ident[EI_MAG0] != ELFMAG0 || header.e_ident[EI_MAG1] != ELFMAG1
        || header.e_ident[EI_MAG2] != ELFMAG2
        || header.e_ident[EI_MAG3] != ELFMAG3) {
        fprintf(stderr, "%s: not an elf file\n", program_name);
        return EXIT_FAILURE;
    }

    unsigned char class = header.e_ident[EI_CLASS];
    if (class != ELFCLASS64) {
        fprintf(stderr, "%s: unsupported architecture: %u\n", program_name,
                class);
        return EXIT_FAILURE;
    }

    unsigned char data = header.e_ident[EI_DATA];
    if (data != ELFDATA2LSB) {
        fprintf(stderr, "%s: unsupported endianness: %u\n", program_name, data);
        return EXIT_FAILURE;
    }

    unsigned char version = header.e_ident[EI_VERSION];
    if (version != EV_CURRENT) {
        fprintf(stderr, "%s: unsupported ELF version: %u\n", program_name,
                version);
        return EXIT_FAILURE;
    }

    unsigned char abi = header.e_ident[EI_OSABI];
    if (abi != UVM_ABI) {
        fprintf(stderr, "%s: unsupported OS abi: %u\n", program_name, abi);
        return EXIT_FAILURE;
    }

    unsigned char abiversion = header.e_ident[EI_ABIVERSION];
    if (abiversion != 0) {
        fprintf(stderr, "%s: unsupported OS abi version: %u\n", program_name,
                abiversion);
        return EXIT_FAILURE;
    }

    unsigned char object_type = header.e_type;
    if (object_type != ET_DYN) {
        fprintf(stderr, "%s: not a shared library: %u\n", program_name,
                object_type);
#ifdef UVM_STRICT
        return EXIT_FAILURE;
#endif
    }

    unsigned char machine = header.e_machine;
    if (machine != UVM_ARCH) {
        fprintf(stderr, "%s: wrong architecture: %u\n", program_name, machine);
#ifdef UVM_STRICT
        return EXIT_FAILURE;
#endif
    }

    unsigned char version_2 = header.e_version;
    if (version_2 != EV_CURRENT) {
        fprintf(stderr, "%s: unsupported ELF version: %u\n", program_name,
                version_2);
        return EXIT_FAILURE;
    }

    Elf64_Addr entry_point = from_le_64(header.e_entry);
    Elf64_Off program_headers_start = from_le_64(header.e_phoff);
    Elf64_Off section_headers_start = from_le_64(header.e_shoff);

    uint32_t flags = from_le_32(header.e_flags);
    if (flags != 0) {
        fprintf(stderr, "%s: unsupported ELF processor flags: %" PRIu32 "\n",
                program_name, flags);
        return EXIT_FAILURE;
    }

    uint16_t hsize = from_le_16(header.e_ehsize);
    if (hsize != sizeof(Elf64_Ehdr)) {
        fprintf(stderr, "%s: ELF header size is wrong: %" PRIu16 "\n",
                program_name, hsize);
        return EXIT_FAILURE;
    }

    uint16_t phentsize = from_le_16(header.e_phentsize);
    if (phentsize != sizeof(Elf64_Phdr)) {
        fprintf(stderr,
                "%s: unexpected program header entry size: %" PRIu16 "\n",
                program_name, phentsize);
        return EXIT_FAILURE;
    }

    uint16_t phnum = from_le_16(header.e_phnum);
    if (phnum >= PN_XNUM) {
        fprintf(stderr, "%s: extra large amount of program headers is "
                        "currently unimplemented\n",
                program_name);
        return EXIT_FAILURE;
    }

    uint16_t shentsize = from_le_16(header.e_shentsize);
    if (shentsize != sizeof(Elf64_Shdr)) {
        fprintf(stderr,
                "%s: unexpected section header entry size: %" PRIu16 "\n",
                program_name, shentsize);
        return EXIT_FAILURE;
    }

    uint16_t shnum = from_le_16(header.e_shnum);
    if (shnum >= SHN_LORESERVE) {
        fprintf(stderr, "%s: file uses reserved shnum values: %" PRIu16 "\n",
                program_name, shnum);
        return EXIT_FAILURE;
    }
    if (0U == shnum) {
        fprintf(stderr, "%s: extra large amount of section headers is "
                        "currently unimplemented\n",
                program_name);
        return EXIT_FAILURE;
    }

    uint16_t shstrndx = from_le_16(header.e_shstrndx);
    if (SHN_XINDEX == shstrndx) {
        fprintf(stderr, "%s: extra large amount of section headers is "
                        "currently unimplemented\n",
                program_name);
        return EXIT_FAILURE;
    }

    size_t phtotal = phentsize * (size_t)phnum;
    if (phtotal > MAXIMUM_PHSIZE) {
        fprintf(stderr, "%s: the program headers are strangely large: %zu\n",
                program_name, phtotal);
        return EXIT_FAILURE;
    }

    size_t shtotal = shentsize * (size_t)shnum;
    if (phtotal > MAXIMUM_PHSIZE) {
        fprintf(stderr, "%s: the section headers are strangely large: %zu\n",
                program_name, shtotal);
        return EXIT_FAILURE;
    }

    struct segment *program_segments = calloc(phnum, sizeof *program_segments);
    if (0 == program_segments) {
        perror("calloc");
        return EXIT_FAILURE;
    }

    Elf64_Phdr *pheaders = malloc(phtotal);
    if (0 == pheaders) {
        perror("malloc");
        return EXIT_FAILURE;
    }

    Elf64_Shdr *sheaders = malloc(shtotal);
    if (0 == sheaders) {
        perror("malloc");
        return EXIT_FAILURE;
    }

    if (-1 == fseek(f, program_headers_start, SEEK_SET)) {
        perror("fseek");
        return EXIT_FAILURE;
    }

    {
        size_t bytes = fread(pheaders, 1, phtotal, f);
        if (bytes < phtotal) {
            if (ferror(f)) {
                perror("fread");
            } else {
                fprintf(stderr, "%s: short file: %zu\n", program_name, bytes);
            }
            return EXIT_FAILURE;
        }
    }

    uintptr_t aslr_offset;
    {
    retry_loading:;
        long page_size = sysconf(_SC_PAGESIZE);
        aslr_offset = (random() / page_size) * page_size;

        fprintf(stderr, "%s: random offset: %" PRIu64 "\n", program_name,
                aslr_offset);

        size_t ii = 0U;
        for (; ii < phnum; ++ii) {
            Elf64_Phdr pheader = pheaders[ii];
            uint32_t type = from_le_32(pheader.p_type);
            uint32_t flags = from_le_32(pheader.p_flags);
            Elf64_Off offset = from_le_64(pheader.p_offset);
            Elf64_Addr vaddr = from_le_64(pheader.p_vaddr);
            Elf64_Addr paddr = from_le_64(pheader.p_paddr);
            uint64_t filesz = from_le_64(pheader.p_filesz);
            uint64_t memsz = from_le_64(pheader.p_memsz);
            uint64_t align = from_le_64(pheader.p_align);

            if (PT_NULL == type)
                continue;

            if (paddr != 0U) {
                fprintf(stderr, "%s: nonzero physical address: %" PRIu64 "\n",
                        program_name, paddr);
#ifdef UVM_STRICT
                return EXIT_FAILURE;
#endif
            }

            if ((flags & ~(PF_X | PF_R | PF_W)) != 0U) {
                fprintf(stderr,
                        "%s: unknown program header flags: %" PRIu32 "\n",
                        program_name, flags);
                return EXIT_FAILURE;
            }

            switch (type) {
            case PT_LOAD: {
                bool is_executable = (flags & PF_X) != 0U;
                bool is_readable = (flags & PF_R) != 0U;
                bool is_writable = (flags & PF_W) != 0U;

                int prot = 0;
                if (is_executable)
                    prot |= PROT_EXEC;
                if (is_readable)
                    prot |= PROT_READ;
                if (is_writable)
                    prot |= PROT_WRITE;
                if (0 == prot)
                    prot = PROT_NONE;

                if (memsz < filesz) {
                    fprintf(stderr,
                            "%s: strangely low memory size: %" PRIu64 "\n",
                            program_name, memsz);
                    return EXIT_FAILURE;
                }

                size_t low = (paddr / page_size) * page_size;
                void *allocate_addr = (void *)(aslr_offset + low);
                void *actual_addr = (void *)(aslr_offset + paddr);
                void *mem = mmap(allocate_addr, memsz, PROT_READ | PROT_WRITE,
                                 MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
                if (MAP_FAILED == mem) {
                    perror("mmap");
                    return EXIT_FAILURE;
                }

                if (-1 == fseek(f, offset, SEEK_SET)) {
                    perror("fseek");
                    return EXIT_FAILURE;
                }
                if (((uintptr_t)mem) == (uintptr_t)allocate_addr) {
                    size_t bytes = fread(actual_addr, 1, filesz, f);
                    if (bytes < filesz) {
                        if (ferror(f)) {
                            perror("fread");
                            return EXIT_FAILURE;
                        } else {
                            fprintf(stderr, "%s: short file: %zu\n",
                                    program_name, bytes);
                        }
                    }
                }

                if (-1 == mprotect(mem, memsz, prot)) {
                    perror("mprotect");
                    return EXIT_FAILURE;
                }

                program_segments[ii].addr = mem;
                program_segments[ii].size = memsz;

                fprintf(stderr, "allocated at %p %zu\n", mem, memsz);

                if (((uintptr_t)mem) != (uintptr_t)allocate_addr) {
                    goto abort_loading;
                }
                break;
            }

            default:
                fprintf(stderr,
                        "%s: unknown program header type: %" PRIu32 "\n",
                        program_name, type);
#ifdef UVM_STRICT
                return EXIT_FAILURE;
#endif
                break;
            }
        }
        goto finish_loading;

    abort_loading:
        for (size_t jj = 0; jj <= ii; ++jj) {
            munmap(program_segments[ii].addr, program_segments[ii].size);
        }
        goto retry_loading;

    finish_loading:;
    }

    fprintf(stderr, "pid: %i, aslr_offset: 0x%X, entry: 0x%X\n", getpid(),
            aslr_offset, entry_point);

    uvm_run((void *)aslr_offset, entry_point);

    return EXIT_SUCCESS;
}

static uint64_t from_le_64(uint64_t x)
{
    unsigned char bytes[8U];
    assert(sizeof bytes == sizeof x);
    memcpy(bytes, &x, sizeof bytes);

    return ((uint64_t)bytes[0U]) | ((uint64_t)bytes[1U]) << 8U
           | ((uint64_t)bytes[2U]) << 16U | ((uint64_t)bytes[3U]) << 24U
           | ((uint64_t)bytes[4U]) << 32U | ((uint64_t)bytes[5U]) << 40U
           | ((uint64_t)bytes[6U]) << 48U | ((uint64_t)bytes[7U]) << 56U;
}

static uint32_t from_le_32(uint32_t x)
{
    unsigned char bytes[4U];
    assert(sizeof bytes == sizeof x);
    memcpy(bytes, &x, sizeof bytes);

    return ((uint32_t)bytes[0U]) | ((uint32_t)bytes[1U]) << 8U
           | ((uint32_t)bytes[2U]) << 16U | ((uint32_t)bytes[3U]) << 24U;
}

static uint16_t from_le_16(uint16_t x)
{
    unsigned char bytes[2U];
    assert(sizeof bytes == sizeof x);
    memcpy(bytes, &x, sizeof bytes);

    return ((uint16_t)bytes[0U]) | ((uint16_t)bytes[1U]) << 8U;
}
