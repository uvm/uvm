#[macro_use]
extern crate nom;
mod elf;

use elf::*;

use std::mem;
use std::io;
use std::slice;
use std::boxed::Box;
use std::io::Write;

use std::borrow::Borrow;
use std::io::Read;
use std::str;
use std::str::from_utf8;
use std::char::from_digit;

use nom::{IResult, alpha, alphanumeric, digit};

named!(identifier<&[u8], String>,
       chain!(
           h: map_res!(alpha, from_utf8) ~
           t: many0!(map_res!(alt!( tag!("_") | alphanumeric), from_utf8)),
           || {
               t.into_iter().fold(h.to_string(), |mut accum, slice| {
                   accum.push_str(slice);
                   accum
})}));

#[derive(Debug)]
struct Constant {
    name: String,
    value: Value,
}

named!(u64_constant<&[u8], Constant>,
       ws!(do_parse!(name : identifier >>
                     tag!(":") >>
                     tag!(".quad") >>
                     value: do_parse!(nums: digit >>
                                      ({
                                          let l = nums.len();
                                          nums.iter().enumerate().map(|(ix, d)|  (*d - 48u8) as u64 * (10u8 as u64).pow((l as u32) - (ix as u32) - (1u8 as u32))).sum()
                                      })
                     ) >>
                     (Constant { name : name, value: Value::Quad(value)}))));

named!(string<&[u8], Constant>,
       ws!(do_parse!(name : identifier >>
                     tag!(":") >>
                     tag!(".asciz") >>
                     tag!("\"") >>
                     str: many0!(map_res!(is_not!("\""), from_utf8)) >>
                     tag!("\"") >>
                     (Constant { name : name,  value : Value::String(
                         str.into_iter().fold("".to_string(), |mut accum, slice| {
                             accum.push_str(slice);
                             accum
                         }))}))));

named!(data<&[u8], Vec<Constant>>,
       ws!(do_parse!(tag!(".data") >> vv : many0!(alt!(u64_constant | string)) >> (vv))));


#[derive(Debug)]
enum Bits {
    Setquad { pointer: u8, src: u8 },
    Getquad { dest: u8, src: u8 },
    Add { dest: u8, src: u8 },
    Subtract { dest: u8, src: u8 },
    Multiply { dest: u8, src: u8 },
    Divide { dest: u8, src: u8 },
    And { dest: u8, src: u8 },
    Or { dest: u8, src: u8 },
    Xor { dest: u8, src: u8 },
}

#[derive(Debug)]
enum Instruction {
    Debug,
    Load { register: u8, constant: String },
    Loadref { register: u8, constant: String },
    Call { constant: String },
    Return,
    Move_Op { dest: u8, src: u8 },
    Retcall { constant: String },
    Retcallifnotzero { register: u8, constant: String },
    New { register: u8, bytes: u8 },
    Bits64(Bits),
}
impl Instruction {
    fn size(&self) -> usize {
        match self {
            &Instruction::Load { ref register, ref constant } => 1 + 1 + 2,
            &Instruction::Loadref { ref register, ref constant } => 1 + 1 + 2,
            &Instruction::Call { ref constant } => 1 + 2,
            &Instruction::Debug => 1,
            &Instruction::Return => 1 + 1,
            &Instruction::Move_Op { ref dest, ref src } => 1 + 1,
            &Instruction::Retcall { ref constant } => 1 + 2,
            &Instruction::Retcallifnotzero { ref register, ref constant } => 1 + 1 + 2,
            &Instruction::New { ref register, ref bytes } => 1 + 1,
            &Instruction::Bits64(_) => 1 + 1,
        }
    }
}

named!(register<&[u8], u8>,
       do_parse!(tag!("%r") >>
                 value: do_parse!(nums: digit >>
                                  ({
                                      let l = nums.len();
                                      let r = nums.iter().enumerate().map(|(ix, d)|  (*d - 48u8) * 10u8.pow((l as u32) - (ix as u32) - (1u8 as u32))).sum();
                                      if r > 5 {
                                          panic!()
                                      }
                                      r
                                  })
                 ) >>
                 (value)));
named!(loadref<&[u8], Instruction>,
       ws!(do_parse!(
                     tag!("loadref") >>
                     register: register >>
                     constant: identifier >>
                     (Instruction::Loadref { register: register , constant: constant}))));
named!(load<&[u8], Instruction>,
       ws!(do_parse!(tag!("load") >>
                     register: register >>
                     constant: identifier >>
                     (Instruction::Load { register: register , constant: constant}))));
named!(call<&[u8], Instruction>,
       ws!(do_parse!(tag!("call") >>
                     constant: identifier >>
                     (Instruction::Call { constant: constant}))));
named!(retcall<&[u8], Instruction>,
       ws!(do_parse!(tag!("retcall") >>
                     constant: identifier >>
                     (Instruction::Retcall { constant: constant}))));
named!(retcallifnotzero<&[u8], Instruction>,
       ws!(do_parse!(tag!("retcallifnotzero") >>
                     register: register >>
                     constant: identifier >>
                     (Instruction::Retcallifnotzero { register: register, constant: constant}))));
named!(return_instr<&[u8], Instruction>,
       ws!(do_parse!(tag!("return") >>
                     (Instruction::Return))));
named!(move_op<&[u8], Instruction>,
       ws!(do_parse!(tag!("move") >>
                     dest: register >>
                     src: register >>
                     (Instruction::Move_Op { dest: dest , src: src}))));
named!(debug<&[u8], Instruction>,
       ws!(do_parse!(tag!("debug") >>
                     (Instruction::Debug))));

named!(add<&[u8], Instruction>,
       ws!(do_parse!(tag!("add") >>
                     dest: register >>
                     src: register >>
                     (Instruction::Bits64(Bits::Add {dest: dest, src: src })))));

named!(sub<&[u8], Instruction>,
       ws!(do_parse!(tag!("sub") >>
                     dest: register >>
                     src: register >>
                     (Instruction::Bits64(Bits::Subtract {dest: dest, src: src})))));

named!(xor<&[u8], Instruction>,
       ws!(do_parse!(tag!("xor") >>
                     dest: register >>
                     src: register >>
                     (Instruction::Bits64(Bits::Xor {dest: dest, src: src })))));

named!(getquad<&[u8], Instruction>,
       ws!(do_parse!(tag!("getquad") >>
                     dest: register >>
                     src: register >>
                     (Instruction::Bits64(Bits::Getquad {dest: dest, src: src })))));

named!(setquad<&[u8], Instruction>,
       ws!(do_parse!(tag!("setquad") >>
                     pointer: register >>
                     src: register >>
                     (Instruction::Bits64(Bits::Setquad {pointer: pointer, src: src })))));

named!(new<&[u8], Instruction>,
       ws!(do_parse!(tag!("new") >>
                     register: register >>
                     bytes: do_parse!(nums: digit >>
                                  ({
                                      let l = nums.len();
                                      let r = nums.iter().enumerate().map(|(ix, d)|  (*d - 48u8) * 10u8.pow((l as u32) - (ix as u32) - (1u8 as u32))).sum();
                                      if r == 0 {
                                          panic!()
                                      }
                                      if r % 2 != 0 {
                                          panic!()
                                      }
                                      r
                                  })
                     ) >>
                     (Instruction::New {register: register, bytes: bytes }))));

named!(instruction<&[u8], Instruction>, alt!(setquad | getquad | new | loadref | load | call | retcallifnotzero | retcall  | return_instr | move_op | add | sub | xor | debug));

#[derive(Debug)]
struct Code {
    name: String,
    instructions: Vec<Instruction>,
}
named!(code<&[u8], Code>,
       ws!(do_parse!(name: identifier >>
                     tag!(":") >>
                     instructions: many1!(do_parse!(x : instruction >> (x))) >>
                     (Code { name: name, instructions: instructions}))));
named!(code_list<&[u8], Vec<Code>>,
       ws!(do_parse!(tag!(".text") >>
                     vv : many0!(code)
                     >> (vv))));

#[derive(Debug)]
struct Source {
    data: Vec<Constant>,
    code: Vec<Code>,
}

named!(quoted<&[u8], String>,
       do_parse!(tag!("\"") >>
                     str: many0!(map_res!(is_not!("\""), from_utf8)) >>
                     tag!("\"") >>
                     (
                         str.into_iter().fold("".to_string(), |mut accum, slice| {
                             accum.push_str(slice);
                             accum
                         }))));

enum Directive {
    Text,
    Data,
    File(String),
    Quad(u64),
}
named!(text<&[u8], Directive>, ws!(do_parse!(tag!(".text") >>
                     (Directive::Text))));
// named!(data<&[u8], Directive>, ws!(do_parse!(tag!(".data") >>
//                      (Directive::Data))));
named!(file<&[u8], Directive>, ws!(do_parse!(tag!(".file") >>
                     str: quoted >>
                     (Directive::File (str)))));
named!(quad<&[u8], Directive>, ws!(do_parse!(tag!(".quad") >>
                     val : do_parse!(nums: digit >>
                                      ({
                                          let l = nums.len();
                                          nums.iter().enumerate().map(|(ix, d)|  (*d - 48u8) as u64 * (10u8 as u64).pow((l as u32) - (ix as u32) - (1u8 as u32))).sum()
                                      })
                     ) >>
                     (Directive::Quad (val)))));
named!(directive<&[u8], Directive>, alt!(file | text));

named!(directives<&[u8], Vec<Directive>>,
       ws!(do_parse!(vv : many1!(directive) >> (vv))));

named!(source<&[u8], Source>,
       ws!(do_parse!(data : opt!(data) >>
                     code_list : opt!(code_list) >>
                     eof!() >>
                     (Source {
                              data : data.unwrap_or(Vec::new()),
                              code : code_list.unwrap_or(Vec::new())
                     }))));
#[derive(Debug)]
struct LinkedSignature {
    ret_type: u16,
    arg_types: Vec<u16>,
}

#[derive(Debug)]
struct LinkedNativeCode {
    signature: u16,
    binding: String,
}

#[derive(Debug)]
enum LinkedInstruction {
    Nop { register: u8, constant: i16 },
    Debug,
    Return,
    Call { constant: i16 },
    Load { register: u8, constant: i16 },
    Loadref { register: u8, constant: i16 },
    Move_Op { dest: u8, src: u8 },
    Retcall { constant: i16 },
    Retcallifnotzero { register: u8, constant: i16 },
    New { register: u8, bytes: u8 },
    Bits64(Bits),
}

#[derive(Debug)]
struct LinkedExport {
    name: String,
    value: u16,
}

#[derive(Debug)]
struct Label {
    name: String,
    ix: usize,
    size: usize,
}

#[derive(Debug)]
struct LinkedSource {
    data_labels: Vec<Label>,
    data: Vec<u8>,
    code_labels: Vec<Label>,
    code: Vec<LinkedInstruction>,
}

const MAGIC_NUMBER: u32 = 0x0BADF00Du32;
const MINOR_VERSION: u16 = 0xFFFFu16;
const MAJOR_VERSION: u16 = 0xFFFFu16;


const UVM_OP_NOP: u8 = 0;

const UVM_OP_RETURN: u8 = 1;

const UVM_OP_CALL: u8 = 2;

const UVM_OP_RETCALL: u8 = 3;
const UVM_OP_RETCALLIFNOTZERO: u8 = 4;


const UVM_OP_LOAD: u8 = 5;

const UVM_OP_MOVE: u8 = 6;

const UVM_OP_ADD_64: u8 = 7;
const UVM_OP_SUBTRACT_64: u8 = 8;
const UVM_OP_MULTIPLY_64: u8 = 9;
const UVM_OP_DIVIDE_64: u8 = 10;
const UVM_OP_AND_64: u8 = 11;
const UVM_OP_OR_64: u8 = 12;
const UVM_OP_XOR_64: u8 = 13;

const UVM_OP_LOADREF: u8 = 14;
const UVM_OP_DEBUG: u8 = 15;
const UVM_OP_NEW: u8 = 16;
const UVM_OP_GETQUAD: u8 = 17;
const UVM_OP_SETQUAD: u8 = 18;

fn uvm_u64(n: u64) -> [u8; 8] {
    [(n & 0xFFu64) as u8,
     ((n >> 8u64) & 0xFFu64) as u8,
     ((n >> 16u64) & 0xFFu64) as u8,
     ((n >> 24u64) & 0xFFu64) as u8,
     ((n >> 32u64) & 0xFFu64) as u8,
     ((n >> 40u64) & 0xFFu64) as u8,
     ((n >> 48u64) & 0xFFu64) as u8,
     ((n >> 56u64) & 0xFFu64) as u8]
}

fn uvm_u32(n: u32) -> [u8; 4] {
    [(n & 0xFFu32) as u8,
     ((n >> 8u32) & 0xFFu32) as u8,
     ((n >> 16u32) & 0xFFu32) as u8,
     ((n >> 24u32) & 0xFFu32) as u8]
}

fn uvm_u16(n: u16) -> [u8; 2] {
    [(n & 0xFFu16) as u8, ((n >> 8u16) & 0xFFu16) as u8]
}
fn uvm_i16(n: i16) -> [u8; 2] {
    [(n & 0xFFi16) as u8, ((n >> 8u16) & 0xFFi16) as u8]
}
fn lookup(constant_pool: &Vec<Label>, name: &String, pc: usize) -> i16 {
    match constant_pool.iter().find(|x| name == &x.name) {
        None => panic!("cannot find {:?} in {:?}", name, constant_pool),
        Some(x) => x.ix as i16 - (pc as i16),
    }
}

fn lookup_fn(constant_pool: &Vec<Label>, name: &String, pc: usize) -> i16 {
    match constant_pool.iter().find(|x| name == &x.name) {
        None => panic!("cannot find {:?} in {:?}", name, constant_pool),
        Some(x) => x.ix as i16 - (pc as i16),
    }
}

fn link(source: Source) -> LinkedSource {
    let Source { data, code } = source;
    let mut text_size = 0;
    let mut code_ix = Vec::new();
    for &Code { ref instructions, ref name } in code.iter() {
        let mut fn_size = 0;
        for x in instructions.iter() {
            fn_size += x.size();
        }
        code_ix.push(Label {
            ix: text_size,
            name: name.clone(),
            size: fn_size,
        });
        text_size += fn_size
    }

    let mut data_ix = text_size;
    let mut data_labels = Vec::new();
    let mut linked_data = Vec::new();
    for c in data.iter() {
        match c.value {
            Value::String(ref s) => {
                linked_data.extend(s.as_bytes().iter());
                linked_data.push(0);
                data_labels.push(Label {
                    name: c.name.clone(),
                    ix: data_ix,
                    size: s.as_bytes().len() + 1,
                });
                data_ix += s.as_bytes().len() + 1;
            }
            Value::Quad(v) => {
                linked_data.extend(uvm_u64(v).iter());
                data_labels.push(Label {
                    name: c.name.clone(),
                    ix: data_ix,
                    size: 8,
                });
                data_ix += 8;
            }
        }
    }

    let mut linked_instructions = Vec::new();
    let mut pc = 0;
    for c in code {
        let Code { name, instructions } = c;
        for instr in instructions {
            let instr_size = instr.size();
            match instr {
                Instruction::Load { register, constant } => {
                    let ix = lookup(&data_labels, &constant, pc);
                    linked_instructions.push(LinkedInstruction::Load {
                        register: register,
                        constant: ix,
                    });
                }
                Instruction::Loadref { register, constant } => {
                    let ix = lookup(&data_labels, &constant, pc);
                    linked_instructions.push(LinkedInstruction::Loadref {
                        register: register,
                        constant: ix,
                    });
                }
                Instruction::Call { constant } => {
                    let ix = lookup_fn(&code_ix, &constant, pc);
                    linked_instructions.push(LinkedInstruction::Call { constant: ix });
                }
                Instruction::Retcall { constant } => {
                    let ix = lookup_fn(&code_ix, &constant, pc);
                    linked_instructions.push(LinkedInstruction::Retcall { constant: ix });
                }
                Instruction::Retcallifnotzero { register, constant } => {
                    let ix = lookup_fn(&code_ix, &constant, pc);
                    linked_instructions.push(LinkedInstruction::Retcallifnotzero {
                        register: register,
                        constant: ix,
                    });
                }
                Instruction::Return => {
                    linked_instructions.push(LinkedInstruction::Return);
                }
                Instruction::Debug => {
                    linked_instructions.push(LinkedInstruction::Debug);
                }
                Instruction::Move_Op { dest, src } => {
                    linked_instructions.push(LinkedInstruction::Move_Op {
                        dest: dest,
                        src: src,
                    });
                }
                Instruction::New { register, bytes } => {
                    linked_instructions.push(LinkedInstruction::New {
                        register: register,
                        bytes: bytes,
                    });
                }
                Instruction::Bits64(bits) => {
                    linked_instructions.push(LinkedInstruction::Bits64(bits));
                }
            }
            pc += instr_size;
        }
    }

    LinkedSource {
        data_labels: data_labels,
        data: linked_data,
        code_labels: code_ix,
        code: linked_instructions,
    }
}

fn encode_reg_constant(op: u8, reg: u8, con: i16) -> [u8; 4] {
    [op, reg, (con & 0xFFi16) as u8, ((con >> 8u16) & 0xFFi16) as u8]
}
fn encode_constant(op: u8, con: i16) -> [u8; 3] {
    [op, (con & 0xFFi16) as u8, ((con >> 8u16) & 0xFFi16) as u8]
}
fn encode_reg_reg(op: u8, dest: u8, src: u8) -> [u8; 2] {
    [op, dest | src << 3]
}

#[derive(Clone)]
struct Section {
    name: String,
    sh_type: u32,
    sh_flags: u64,
    sh_link: u32,
    sh_info: u32,
    entry_size: u64,
    contents: Box<[u8]>,
}
#[derive(Debug)]
enum Value {
    String(String),
    Quad(u64),
}
struct Data {
    name: String,
    value: Value,
}

// Bogus
const UVM_ABI: u8 = 0xFF;
const UVM_ARCH: u16 = 0xFF;
const R_UVM_COPY: u32 = 1;

fn elf_link(sections: Box<[Section]>) {
    let stdout = io::stdout();
    let mut l = stdout.lock();

    let mut header = Elf64_Ehdr::default();
    header.e_ident[EI_MAG0] = ELFMAG0;
    header.e_ident[EI_MAG1] = 'E' as u8;
    header.e_ident[EI_MAG2] = 'L' as u8;
    header.e_ident[EI_MAG3] = 'F' as u8;
    header.e_ident[EI_CLASS] = ELFCLASS64;
    header.e_ident[EI_DATA] = ELFDATA2LSB;
    header.e_ident[EI_VERSION] = EV_CURRENT;

    header.e_ident[EI_OSABI] = UVM_ABI;
    header.e_ident[EI_ABIVERSION] = 0;

    header.e_type = ET_DYN as u16;

    header.e_machine = UVM_ARCH;
    header.e_version = EV_CURRENT as u32;

    let program_entry = 0;

    let program_header_offset = mem::size_of::<Elf64_Ehdr>();
    let program_header_entry_size = mem::size_of::<Elf64_Phdr>();
    let number_of_program_headers = 1usize;

    let program_header_end = program_header_offset +
                             number_of_program_headers * program_header_entry_size;

    let section_header_offset = program_header_end;
    let section_header_entry_size = mem::size_of::<Elf64_Shdr>();
    let number_of_section_headers = sections.len() + 1;

    header.e_entry = program_entry as u64;
    header.e_phoff = program_header_offset as u64;
    header.e_shoff = section_header_offset as u64;

    header.e_ehsize = mem::size_of::<Elf64_Ehdr>() as u16;

    header.e_phentsize = program_header_entry_size as u16;
    header.e_phnum = number_of_program_headers as u16;

    header.e_shentsize = section_header_entry_size as u16;
    header.e_shnum = number_of_section_headers as u16;

    header.e_shstrndx = sections.len() as u16;

    {
        let p: *const Elf64_Ehdr = &header;
        let p: *const u8 = p as *const u8;
        let s: &[u8] = unsafe { slice::from_raw_parts(p, mem::size_of::<Elf64_Ehdr>()) };
        l.write(s);
    }
    let mut secsize = 0;
    for section in sections.iter() {
        secsize += section.contents.len();
    }
    {
        let mut phdr = Elf64_Phdr::default();
        phdr.p_type = PT_LOAD as u32;
        phdr.p_offset =
            (section_header_offset + number_of_section_headers * section_header_entry_size) as u64;
        phdr.p_vaddr = 0;
        phdr.p_paddr = 0;
        phdr.p_filesz = secsize as u64;
        phdr.p_memsz = secsize as u64;
        phdr.p_flags = PF_R as u32 | PF_X as u32;
        phdr.p_align = 0;

        let p: *const Elf64_Phdr = &phdr;
        let p: *const u8 = p as *const u8;
        let s: &[u8] = unsafe { slice::from_raw_parts(p, mem::size_of::<Elf64_Phdr>()) };
        l.write(s);
    }

    let mut section_contents = Vec::new();
    let mut names: Vec<String> = Vec::new();

    let mut secstart: usize = section_header_offset +
                              number_of_section_headers * mem::size_of::<Elf64_Shdr>();

    for section in sections.iter() {
        let name_ix = names.iter().map(|x| x.as_bytes().len() + 1).sum::<usize>() + 1;

        names.push(section.name.clone());

        let size = section.contents.len();

        {
            let mut shdr = Elf64_Shdr::default();
            shdr.sh_name = (name_ix as u32).to_le();
            shdr.sh_type = section.sh_type.to_le();
            shdr.sh_offset = (secstart as u64).to_le();
            shdr.sh_entsize = section.entry_size.to_le();
            shdr.sh_flags = section.sh_flags.to_le();
            shdr.sh_info = section.sh_info.to_le();
            shdr.sh_link = section.sh_link.to_le();
            shdr.sh_size = (size as u64).to_le();

            let p: *const Elf64_Shdr = &shdr;
            let p: *const u8 = p as *const u8;
            let s: &[u8] = unsafe { slice::from_raw_parts(p, mem::size_of::<Elf64_Shdr>()) };
            l.write(s);
        }
        secstart += size;

        section_contents.push(section.contents.clone());
    }

    {
        let name: usize = names.iter().map(|x| x.as_bytes().len() + 1).sum::<usize>() + 1;

        names.push(".shstrtab".to_string());

        let size = names.iter().map(|x| x.as_bytes().len() + 1).sum::<usize>() + 1;

        let mut shdr = Elf64_Shdr::default();
        shdr.sh_name = (name as u32).to_le();
        shdr.sh_type = (SHT_STRTAB as u32).to_le();
        shdr.sh_offset = (secstart as u64).to_le();
        shdr.sh_flags = (SHF_STRINGS as u64).to_le();
        shdr.sh_entsize = 0;
        shdr.sh_size = (size as u64).to_le();

        let p: *const Elf64_Shdr = &shdr;
        let p: *const u8 = p as *const u8;
        let s: &[u8] = unsafe { slice::from_raw_parts(p, mem::size_of::<Elf64_Shdr>()) };
        l.write(s);
    }

    for content in section_contents.iter() {
        l.write(content);
    }

    l.write(&[0]);
    for name in names.iter().map(|x| x.as_bytes()) {
        l.write(name);
        l.write(&[0]);
    }

}

fn main() {
    let mut s = Vec::new();
    io::stdin().read_to_end(&mut s);

    match source(s.into_boxed_slice().borrow()) {
        IResult::Done(_, source) => {
            let c = link(source);

            let mut instr_stream = Vec::new();
            for instr in c.code.iter() {
                match instr {
                    &LinkedInstruction::Load { register, constant } => {
                        instr_stream.extend(&encode_reg_constant(UVM_OP_LOAD, register, constant));
                    }
                    &LinkedInstruction::Loadref { register, constant } => {
                        instr_stream.extend(&encode_reg_constant(UVM_OP_LOADREF, register, constant));
                    }
                    &LinkedInstruction::Debug => {
                        instr_stream.push(UVM_OP_DEBUG);
                    }
                    &LinkedInstruction::Retcall { constant } => {
                        instr_stream.extend(&encode_constant(UVM_OP_RETCALL, constant));
                    }
                    &LinkedInstruction::Retcallifnotzero { register, constant } => {
                        instr_stream.extend(&encode_reg_constant(UVM_OP_RETCALLIFNOTZERO,
                                                                 register,
                                                                 constant));
                    }
                    &LinkedInstruction::Call { constant } => {
                        instr_stream.extend(&encode_constant(UVM_OP_CALL, constant));
                    }
                    &LinkedInstruction::Return => {
                        instr_stream.push(UVM_OP_RETURN);
                        instr_stream.push(UVM_OP_NOP);
                    }
                    &LinkedInstruction::Move_Op { dest, src } => {
                        instr_stream.extend(&encode_reg_reg(UVM_OP_MOVE, dest, src));
                    }
                    &LinkedInstruction::New { register, bytes } => {
                        let mut b = bytes;
                        let mut n = 0u8;
                        while b != 0 {
                            b /= 2;
                            n += 1;
                        }
                        instr_stream.extend(&encode_reg_reg(UVM_OP_NEW, register, n));
                    }
                    &LinkedInstruction::Bits64(ref bits) => {
                        let (op, dest, src) = match bits {
                            &Bits::Add { dest, src } => (UVM_OP_ADD_64, dest, src),
                            &Bits::Subtract { dest, src } => {
                                // Clear registers using xor instead
                                if dest == src {
                                    panic!();
                                }
                                (UVM_OP_SUBTRACT_64, dest, src)
                            }
                            &Bits::Multiply { dest, src } => (UVM_OP_MULTIPLY_64, dest, src),
                            &Bits::Divide { dest, src } => {
                                // Clear registers using xor instead
                                if dest == src {
                                    panic!();
                                }
                                (UVM_OP_DIVIDE_64, dest, src)
                            }
                            &Bits::And { dest, src } => {
                                // Pointless
                                if dest == src {
                                    panic!();
                                }
                                (UVM_OP_AND_64, dest, src)
                            }
                            &Bits::Or { dest, src } => {
                                // Pointless
                                if dest == src {
                                    panic!();
                                }
                                (UVM_OP_OR_64, dest, src)
                            }
                            &Bits::Xor { dest, src } => (UVM_OP_XOR_64, dest, src),
                            &Bits::Getquad { dest, src } => (UVM_OP_GETQUAD, dest, src),
                            &Bits::Setquad { pointer, src } => (UVM_OP_SETQUAD, pointer, src),
                        };
                        instr_stream.extend(&encode_reg_reg(op, dest, src));
                    }

                    _ => panic!(),
                }
            }

            let mut data_stream = Vec::new();
            data_stream.extend(c.data.iter());

            let mut strtab = Vec::new();
            let mut symtab = Vec::new();
            let mut strix: usize = 1;

            strtab.push(0);

            {
                let sym = Elf64_Sym::default();
                let p: *const Elf64_Sym = &sym;
                let p: *const u8 = p as *const u8;
                let s: &[u8] = unsafe { slice::from_raw_parts(p, mem::size_of::<Elf64_Sym>()) };
                symtab.extend(s.iter());
            }
            for &Label { size, ix, ref name } in c.data_labels.iter() {
                strtab.extend(name.as_bytes().iter());
                strtab.push(0);

                let mut sym = Elf64_Sym::default();
                sym.st_name = strix as u32;
                sym.st_value = ix as u64;
                sym.st_size = size as u64;
                sym.st_info = STT_OBJECT;
                sym.st_other = STV_HIDDEN;
                sym.st_shndx = 2;

                let p: *const Elf64_Sym = &sym;
                let p: *const u8 = p as *const u8;
                let s: &[u8] = unsafe { slice::from_raw_parts(p, mem::size_of::<Elf64_Sym>()) };

                symtab.extend(s.iter());

                strix += name.as_bytes().len() + 1;
            }
            for &Label { size, ix, ref name } in c.code_labels.iter() {
                strtab.extend(name.as_bytes().iter());
                strtab.push(0);

                let mut sym = Elf64_Sym::default();
                sym.st_name = strix as u32;
                sym.st_value = ix as u64;
                sym.st_size = size as u64;
                sym.st_info = STT_OBJECT;
                sym.st_other = STV_HIDDEN;
                sym.st_shndx = 1;

                let p: *const Elf64_Sym = &sym;
                let p: *const u8 = p as *const u8;
                let s: &[u8] = unsafe { slice::from_raw_parts(p, mem::size_of::<Elf64_Sym>()) };

                symtab.extend(s.iter());

                strix += name.as_bytes().len() + 1;
            }

            let mut sections = [Section {
                                    name: "".to_string(),
                                    sh_type: SHT_NULL as u32,
                                    sh_flags: 0,
                                    sh_link: 0,
                                    sh_info: 0,
                                    entry_size: 0,
                                    contents: Vec::new().into_boxed_slice(),
                                },
                                Section {
                                    name: ".text".to_string(),
                                    sh_type: SHT_PROGBITS as u32,
                                    sh_flags: (SHF_ALLOC as u64 | SHF_EXECINSTR as u64),
                                    sh_link: 0,
                                    sh_info: 0,
                                    entry_size: 0,
                                    contents: instr_stream.into_boxed_slice(),
                                },
                                Section {
                                    name: ".rodata".to_string(),
                                    sh_type: SHT_PROGBITS as u32,
                                    sh_flags: SHF_ALLOC as u64,
                                    sh_link: 0,
                                    sh_info: 0,
                                    entry_size: 0,
                                    contents: data_stream.into_boxed_slice(),
                                },
                                Section {
                                    name: ".strtab".to_string(),
                                    sh_type: SHT_STRTAB as u32,
                                    sh_flags: 0,
                                    sh_link: 0,
                                    sh_info: 0,
                                    entry_size: 0,
                                    contents: strtab.into_boxed_slice(),
                                }]
                .to_vec();

            let strtab_ix = sections.iter().position(|x| x.name == ".strtab".to_string()).unwrap();

            sections.push(Section {
                name: ".symtab".to_string(),
                sh_type: SHT_SYMTAB as u32,
                sh_flags: 0,
                sh_link: strtab_ix as u32,
                sh_info: 0,
                entry_size: mem::size_of::<Elf64_Sym>() as u64,
                contents: symtab.into_boxed_slice(),
            });
            elf_link(sections.into_boxed_slice());
        }
        x => {
            writeln!(&mut std::io::stderr(), "{:?}", x);
        }
    }
}
