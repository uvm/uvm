.data
a: .quad 23
b: .quad 49
zero: .quad 0

.text
main:
	load %r1 a
	load %r0 b
	add %r0 %r1
	call print_number
	return

print_number:
	move %r4 %r3
	move %r3 %r2
	move %r2 %r1
	move %r1 %r0
	load %r0 zero
	debug
	move %r0 %r1
	move %r1 %r2
	move %r2 %r3
	move %r3 %r4
	return
