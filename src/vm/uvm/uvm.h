/*
 * Copyright 2014 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef UVM_INCLUDE_H
#define UVM_INCLUDE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

#define UVM_ARRAY_SIZE(...) (sizeof (__VA_ARGS__) / sizeof ((__VA_ARGS__)[0U]))
#define UVM_COUNT(...) (UVM_ARRAY_SIZE((uint8_t[]) { __VA_ARGS__ }))

#define UVM_COUNT_U8(...) UVM_U8(UVM_COUNT(__VA_ARGS__)), __VA_ARGS__
#define UVM_COUNT_U16(...) UVM_U16(UVM_COUNT(__VA_ARGS__)), __VA_ARGS__
#define UVM_COUNT_U32(...) UVM_U32(UVM_COUNT(__VA_ARGS__)), __VA_ARGS__
#define UVM_COUNT_U64(...) UVM_U64(UVM_COUNT(__VA_ARGS__)), __VA_ARGS__

/* This virtual machine is little endian */

#define UVM_U16(X) (X) & 0xFFU, ((X) >> 8U) & 0xFFU

#define UVM_U32(X)                              \
    ((uintmax_t) (X)) & 0xFFU,                  \
        (((uintmax_t) (X)) >> 8U) & 0xFFU,      \
        (((uintmax_t) (X)) >> 16U) & 0xFFU,     \
        (((uintmax_t) (X)) >> 24U) & 0xFFU

#define UVM_U64(X)                              \
    ((uintmax_t) (X)) & 0xFFU,                  \
        (((uintmax_t) (X)) >> 8U) & 0xFFU,      \
        (((uintmax_t) (X)) >> 16U) & 0xFFU,     \
        (((uintmax_t) (X)) >> 24U) & 0xFFU,     \
        (((uintmax_t) (X)) >> 32U) & 0xFFU,     \
        (((uintmax_t) (X)) >> 40U) & 0xFFU,     \
        (((uintmax_t) (X)) >> 48U) & 0xFFU,     \
        (((uintmax_t) (X)) >> 56U) & 0xFFU

#define UVM_MAGIC_NUMBER 0x0BADF00DU

#define UVM_MINOR_VERSION_DEBUG 0xFFFFU
#define UVM_MAJOR_VERSION_DEBUG 0xFFFFU

/* Code constant attribute flags */
#define UVM_CODE_ATTR_NATIVE 1U

/* Public Instructions */
    enum {
        UVM_OP_FIRST,

        UVM_OP_NOP = UVM_OP_FIRST,

	UVM_OP_RETURN,
	UVM_OP_CALL,

        UVM_OP_RETCALL,
        UVM_OP_RETCALLIFNOTZERO,

        UVM_OP_LOAD,

        UVM_OP_MOVE,

        UVM_OP_FIRST_TERNARY,

        UVM_OP_ADD = UVM_OP_FIRST_TERNARY,
        UVM_OP_SUBTRACT,
        UVM_OP_MULTIPLY,
        UVM_OP_DIVIDE,
        UVM_OP_AND,
        UVM_OP_OR,
        UVM_OP_XOR,

        UVM_OP_LAST_TERNARY = UVM_OP_XOR,

        UVM_OP_LOADREF,
	UVM_OP_DEBUG,

        UVM_OP_NEW,
        UVM_OP_GETQUAD,
        UVM_OP_SETQUAD,

        UVM_OP_LAST = UVM_OP_SETQUAD
    };

#if defined _WIN32 || defined __CYGWIN__
#define UVM__IMPORT __declspec(dllimport)
#define UVM__EXPORT __declspec(dllexport)
#elif __GNUC__ >= 4
#define UVM__IMPORT __attribute__ ((visibility ("default")))
#define UVM__EXPORT __attribute__ ((visibility ("default")))
#else
#define UVM__IMPORT
#define UVM__EXPORT
#endif

#ifdef UVM_DLL_EXPORTS
#define UVM__API UVM__EXPORT
#else
#define UVM__API UVM__IMPORT
#endif                          /* UVM_DLL_EXPORTS */

	UVM__API void uvm_run(void const *base, uint16_t entry_point);

#ifndef UVM_DLL_EXPORTS
#undef UVM__IMPORT
#undef UVM__EXPORT
#undef UVM__API
#endif                          /* UVM_DLL_EXPORTS */

#ifdef __cplusplus
}
#endif
#endif                          /* UVM_INCLUDE_H */
