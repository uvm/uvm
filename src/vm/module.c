/*
 * Copyright 2014,2017 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "config.h"

#include "uvm/uvm.h"

#include "_uvm/bytes.h"
#include "_uvm/value.h"

#include "ffi.h"

#include <alloca.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <setjmp.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/* Make the stack limit a megabyte */
#define STACK_LIMIT (1024U * 1024U)

#define ALIGNOF(T)                                                             \
    offsetof(                                                                  \
        struct {                                                               \
            char c;                                                            \
            T h;                                                               \
        },                                                                     \
        h)

#define UINT8_TO_UINT16(A, B)                                                  \
    ((uint16_t)(((uintmax_t)(A)) | (((uintmax_t)(B)) << 8U)))

static char const *instruction_name(uint8_t index);

#define RUN_NEXT_INSTRUCTION(IP)                                               \
    do {                                                                       \
        ctx->instruction_pointer = IP;                                         \
        uint8_t instruction = IP[0U];                                          \
        uint8_t result_reg = IP[1U];                                           \
        opcode_func func = get_opcodes(instruction, result_reg);               \
        if (__builtin_expect(check_stack(ctx), 0))                             \
            op_reset(ctx, reg_0, reg_1, reg_2, reg_3, reg_4);                  \
        func(ctx, reg_0, reg_1, reg_2, reg_3, reg_4);                          \
    } while (0)

#define GET_REG(X, Y)                                                          \
    do {                                                                       \
        switch ((X)) {                                                         \
        case 0U:                                                               \
            Y = reg_0;                                                         \
            break;                                                             \
        case 1U:                                                               \
            Y = reg_1;                                                         \
            break;                                                             \
        case 2U:                                                               \
            Y = reg_2;                                                         \
            break;                                                             \
        case 3U:                                                               \
            Y = reg_3;                                                         \
            break;                                                             \
        case 4U:                                                               \
            Y = reg_4;                                                         \
            break;                                                             \
        default:                                                               \
            __builtin_unreachable();                                           \
        }                                                                      \
    } while (0)

struct context {
    uint8_t const *base;
    uint8_t const *instruction_pointer;
    uvm__value *constants;
    uintptr_t stack_base;
    jmp_buf jump_point;
    uvm__value reg_0;
    uvm__value reg_1;
    uvm__value reg_2;
    uvm__value reg_3;
    uvm__value reg_4;
    bool returning;
};

static inline bool check_stack(struct context *ctx)
{
    uintptr_t sp = (uintptr_t)__builtin_frame_address(0);
    return ctx->stack_base - sp > STACK_LIMIT;
}

typedef void (*opcode_func)(struct context *ctx, uvm__value reg_0,
                            uvm__value reg_1, uvm__value reg_2,
                            uvm__value reg_3, uvm__value reg_4)
    __attribute__((noreturn));

static inline opcode_func get_opcodes(uint8_t instr, uint8_t result_reg);

static uvm__value run(void const *base, uint16_t entry, uvm__value reg_0,
                      uvm__value reg_1, uvm__value reg_2, uvm__value reg_3,
                      uvm__value reg_4);

UVM__API void uvm_run(void const *base, uint16_t entry)
{
    uvm__value val = run(base, entry, 0, 0, 0, 0, 0);
    fprintf(stderr, "val: 0x%" PRIx64 "\n", (uint64_t)val);
}

#define OPCODE_ATTR __attribute__((noreturn)) __attribute__((hot))

/* System V x86_64 ABI passes up to 6 integer or pointers inside
 * registers.  For more registers we either need floating point values
 * or they get pushed to the stack.
 */
OPCODE_ATTR void op_reset(struct context *ctx, uvm__value reg_0, uvm__value reg_1,
                       uvm__value reg_2, uvm__value reg_3, uvm__value reg_4)
{
    ctx->reg_0 = reg_0;
    ctx->reg_1 = reg_1;
    ctx->reg_2 = reg_2;
    ctx->reg_3 = reg_3;
    ctx->reg_4 = reg_4;
    longjmp(ctx->jump_point, 1);
}

OPCODE_ATTR void op_error(struct context *ctx, uvm__value reg_0, uvm__value reg_1,
                       uvm__value reg_2, uvm__value reg_3, uvm__value reg_4)
{
    __builtin_unreachable();
}

/* Public Instructions */
OPCODE_ATTR void op_debug(struct context *ctx, uvm__value reg_0, uvm__value reg_1,
                       uvm__value reg_2, uvm__value reg_3, uvm__value reg_4)
{
    uint8_t const *ip = ctx->instruction_pointer;
    switch (reg_0) {
    case 0:
        fprintf(stderr, "DEBUG: %" PRIu64 "\n", reg_1);
        break;
    case 1:
        fprintf(stderr, "DEBUG: %s\n", (void *)reg_1);
        break;
    }
    ip += 1U;
    RUN_NEXT_INSTRUCTION(ip);
}

OPCODE_ATTR void op_nop(struct context *ctx, uvm__value reg_0, uvm__value reg_1,
                     uvm__value reg_2, uvm__value reg_3, uvm__value reg_4)
{
    uint8_t const *ip = ctx->instruction_pointer;
    ip += 1U;
    RUN_NEXT_INSTRUCTION(ip);
}

OPCODE_ATTR void op_return(struct context *ctx, uvm__value reg_0, uvm__value reg_1,
                        uvm__value reg_2, uvm__value reg_3, uvm__value reg_4)
{
    ctx->returning = true;
    ctx->reg_0 = reg_0;
    longjmp(ctx->jump_point, 1);
}

OPCODE_ATTR void op_retcall(struct context *ctx, uvm__value reg_0,
                         uvm__value reg_1, uvm__value reg_2, uvm__value reg_3,
                         uvm__value reg_4)
{
    uint8_t const *ip = ctx->instruction_pointer;

    void *function = ip + uvm__bytes_to_int16(ip + 1U);

    /* TODO: Check the type */
    uint8_t const *instruction_pointer = function;

    ctx->instruction_pointer = instruction_pointer;
    uint8_t instruction = instruction_pointer[0U];
    uint8_t result_reg = instruction_pointer[1U];
    opcode_func func = get_opcodes(instruction, result_reg);
    if (__builtin_expect(check_stack(ctx), 0))
        op_reset(ctx, reg_0, reg_1, reg_2, reg_3, reg_4);
    func(ctx, reg_0, reg_1, reg_2, reg_3, reg_4);
}

#define OP_RETCALLIFNOTZERO(N)                                                 \
    OPCODE_ATTR void op_retcallifnotzero_##N(                                     \
        struct context *ctx, uvm__value reg_0, uvm__value reg_1,               \
        uvm__value reg_2, uvm__value reg_3, uvm__value reg_4)                  \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        void *function = ip + uvm__bytes_to_int16(ip + 2U);                    \
        ip += 4U;                                                              \
        if (0 == reg_##N)                                                      \
            RUN_NEXT_INSTRUCTION(ip);                                          \
                                                                               \
        /* TODO: Check the type */                                             \
        uint8_t const *instruction_pointer = function;                         \
                                                                               \
        ctx->instruction_pointer = instruction_pointer;                        \
        uint8_t instruction = instruction_pointer[0U];                         \
        uint8_t result_reg = instruction_pointer[1U];                          \
        opcode_func func = get_opcodes(instruction, result_reg);               \
        if (__builtin_expect(check_stack(ctx), 0))                             \
            op_reset(ctx, reg_0, reg_1, reg_2, reg_3, reg_4);                  \
        func(ctx, reg_0, reg_1, reg_2, reg_3, reg_4);                          \
    }
OP_RETCALLIFNOTZERO(0)
OP_RETCALLIFNOTZERO(1)
OP_RETCALLIFNOTZERO(2)
OP_RETCALLIFNOTZERO(3)
OP_RETCALLIFNOTZERO(4)

OPCODE_ATTR void op_call(struct context *ctx, uvm__value reg_0, uvm__value reg_1,
                      uvm__value reg_2, uvm__value reg_3, uvm__value reg_4)
{
    uint8_t const *ip = ctx->instruction_pointer;
    uint16_t function = (ip + uvm__bytes_to_int16(ip + 1U)) - ctx->base;
    ip += 3U;
    uvm__value val
        = run(ctx->base, function, reg_0, reg_1, reg_2, reg_3, reg_4);
    reg_0 = val;

    RUN_NEXT_INSTRUCTION(ip);
}

#define OP_LOADREF(N)                                                          \
    OPCODE_ATTR void op_loadref_##N(struct context *ctx, uvm__value reg_0,        \
                                 uvm__value reg_1, uvm__value reg_2,           \
                                 uvm__value reg_3, uvm__value reg_4)           \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        void *constant_index = ip + uvm__bytes_to_int16(ip + 2U);              \
        ip += 4U;                                                              \
                                                                               \
        reg_##N = (uvm__value)constant_index;                                  \
                                                                               \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_LOADREF(0)
OP_LOADREF(1)
OP_LOADREF(2)
OP_LOADREF(3)
OP_LOADREF(4)

#define OP_LOAD(N)                                                             \
    OPCODE_ATTR void op_load_##N(struct context *ctx, uvm__value reg_0,           \
                              uvm__value reg_1, uvm__value reg_2,              \
                              uvm__value reg_3, uvm__value reg_4)              \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        void *constant_index = ip + uvm__bytes_to_int16(ip + 2U);              \
        ip += 4U;                                                              \
                                                                               \
        reg_##N = uvm__bytes_to_uint64(constant_index);                        \
                                                                               \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_LOAD(0)
OP_LOAD(1)
OP_LOAD(2)
OP_LOAD(3)
OP_LOAD(4)

#define OP_MOVE(M, N)                                                          \
    OPCODE_ATTR void op_move_##M##_##N(struct context *ctx, uvm__value reg_0,     \
                                    uvm__value reg_1, uvm__value reg_2,        \
                                    uvm__value reg_3, uvm__value reg_4)        \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        ip += 2U;                                                              \
        reg_##N = reg_##M;                                                     \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_MOVE(0, 1)
OP_MOVE(0, 2)
OP_MOVE(0, 3)
OP_MOVE(0, 4)
OP_MOVE(1, 0)
OP_MOVE(1, 2)
OP_MOVE(1, 3)
OP_MOVE(1, 4)
OP_MOVE(2, 0)
OP_MOVE(2, 1)
OP_MOVE(2, 3)
OP_MOVE(2, 4)
OP_MOVE(3, 0)
OP_MOVE(3, 1)
OP_MOVE(3, 2)
OP_MOVE(3, 4)
OP_MOVE(4, 0)
OP_MOVE(4, 1)
OP_MOVE(4, 2)
OP_MOVE(4, 3)

#define OP_GETQUAD(M, N)                                                       \
    OPCODE_ATTR void op_getquad_##M##_##N(struct context *ctx, uvm__value reg_0,  \
                                       uvm__value reg_1, uvm__value reg_2,     \
                                       uvm__value reg_3, uvm__value reg_4)     \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        ip += 2U;                                                              \
        memcpy(&reg_##N, (char *)reg_##M, 8U);                                 \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_GETQUAD(0, 0)
OP_GETQUAD(0, 1)
OP_GETQUAD(0, 2)
OP_GETQUAD(0, 3)
OP_GETQUAD(0, 4)
OP_GETQUAD(1, 0)
OP_GETQUAD(1, 1)
OP_GETQUAD(1, 2)
OP_GETQUAD(1, 3)
OP_GETQUAD(1, 4)
OP_GETQUAD(2, 0)
OP_GETQUAD(2, 1)
OP_GETQUAD(2, 2)
OP_GETQUAD(2, 3)
OP_GETQUAD(2, 4)
OP_GETQUAD(3, 0)
OP_GETQUAD(3, 1)
OP_GETQUAD(3, 2)
OP_GETQUAD(3, 3)
OP_GETQUAD(3, 4)
OP_GETQUAD(4, 0)
OP_GETQUAD(4, 1)
OP_GETQUAD(4, 2)
OP_GETQUAD(4, 3)
OP_GETQUAD(4, 4)

#define OP_SETQUAD(M, N)                                                       \
    OPCODE_ATTR void op_setquad_##M##_##N(struct context *ctx, uvm__value reg_0,  \
                                       uvm__value reg_1, uvm__value reg_2,     \
                                       uvm__value reg_3, uvm__value reg_4)     \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        ip += 2U;                                                              \
        memcpy((char *)reg_##N, &reg_##M, 8U);                                 \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_SETQUAD(0, 0)
OP_SETQUAD(0, 1)
OP_SETQUAD(0, 2)
OP_SETQUAD(0, 3)
OP_SETQUAD(0, 4)
OP_SETQUAD(1, 0)
OP_SETQUAD(1, 1)
OP_SETQUAD(1, 2)
OP_SETQUAD(1, 3)
OP_SETQUAD(1, 4)
OP_SETQUAD(2, 0)
OP_SETQUAD(2, 1)
OP_SETQUAD(2, 2)
OP_SETQUAD(2, 3)
OP_SETQUAD(2, 4)
OP_SETQUAD(3, 0)
OP_SETQUAD(3, 1)
OP_SETQUAD(3, 2)
OP_SETQUAD(3, 3)
OP_SETQUAD(3, 4)
OP_SETQUAD(4, 0)
OP_SETQUAD(4, 1)
OP_SETQUAD(4, 2)
OP_SETQUAD(4, 3)
OP_SETQUAD(4, 4)

#define OP_NEW(M, N)                                                           \
    OPCODE_ATTR void op_new_##M##_##N(struct context *ctx, uvm__value reg_0,      \
                                   uvm__value reg_1, uvm__value reg_2,         \
                                   uvm__value reg_3, uvm__value reg_4)         \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        ip += 2U;                                                              \
        char *storage = alloca(2U << M);                                       \
        memset(storage, 0, 2U << M);                                           \
        reg_##N = (uvm__value)storage;                                         \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_NEW(0, 0)
OP_NEW(0, 1)
OP_NEW(0, 2)
OP_NEW(0, 3)
OP_NEW(0, 4)
OP_NEW(1, 0)
OP_NEW(1, 1)
OP_NEW(1, 2)
OP_NEW(1, 3)
OP_NEW(1, 4)
OP_NEW(2, 0)
OP_NEW(2, 1)
OP_NEW(2, 2)
OP_NEW(2, 3)
OP_NEW(2, 4)
OP_NEW(3, 0)
OP_NEW(3, 1)
OP_NEW(3, 2)
OP_NEW(3, 3)
OP_NEW(3, 4)
OP_NEW(4, 0)
OP_NEW(4, 1)
OP_NEW(4, 2)
OP_NEW(4, 3)
OP_NEW(4, 4)

#define OP_ADD(M, N)                                                           \
    OPCODE_ATTR void op_add_##M##_##N(struct context *ctx, uvm__value reg_0,      \
                                   uvm__value reg_1, uvm__value reg_2,         \
                                   uvm__value reg_3, uvm__value reg_4)         \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        ip += 2U;                                                              \
        reg_##N = ((uintmax_t)reg_##N) + ((uintmax_t)reg_##M);                 \
                                                                               \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_ADD(0, 0)
OP_ADD(0, 1)
OP_ADD(0, 2)
OP_ADD(0, 3)
OP_ADD(0, 4)
OP_ADD(1, 0)
OP_ADD(1, 1)
OP_ADD(1, 2)
OP_ADD(1, 3)
OP_ADD(1, 4)
OP_ADD(2, 0)
OP_ADD(2, 1)
OP_ADD(2, 2)
OP_ADD(2, 3)
OP_ADD(2, 4)
OP_ADD(3, 0)
OP_ADD(3, 1)
OP_ADD(3, 2)
OP_ADD(3, 3)
OP_ADD(3, 4)
OP_ADD(4, 0)
OP_ADD(4, 1)
OP_ADD(4, 2)
OP_ADD(4, 3)
OP_ADD(4, 4)

#define OP_SUB(M, N)                                                           \
    OPCODE_ATTR void op_sub_##M##_##N(struct context *ctx, uvm__value reg_0,      \
                                   uvm__value reg_1, uvm__value reg_2,         \
                                   uvm__value reg_3, uvm__value reg_4)         \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        ip += 2U;                                                              \
        reg_##N = ((uintmax_t)reg_##N) - ((uintmax_t)reg_##M);                 \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_SUB(0, 1)
OP_SUB(0, 2)
OP_SUB(0, 3)
OP_SUB(0, 4)
OP_SUB(1, 0)
OP_SUB(1, 2)
OP_SUB(1, 3)
OP_SUB(1, 4)
OP_SUB(2, 0)
OP_SUB(2, 1)
OP_SUB(2, 3)
OP_SUB(2, 4)
OP_SUB(3, 0)
OP_SUB(3, 1)
OP_SUB(3, 2)
OP_SUB(3, 4)
OP_SUB(4, 0)
OP_SUB(4, 1)
OP_SUB(4, 2)
OP_SUB(4, 3)

#define OP_XOR(M, N)                                                           \
    OPCODE_ATTR void op_xor_##M##_##N(struct context *ctx, uvm__value reg_0,      \
                                   uvm__value reg_1, uvm__value reg_2,         \
                                   uvm__value reg_3, uvm__value reg_4)         \
    {                                                                          \
        uint8_t const *ip = ctx->instruction_pointer;                          \
        ip += 2U;                                                              \
        reg_##N = ((uintmax_t)reg_##N) - ((uintmax_t)reg_##M);                 \
        RUN_NEXT_INSTRUCTION(ip);                                              \
    }
OP_XOR(0, 0)
OP_XOR(0, 1)
OP_XOR(0, 2)
OP_XOR(0, 3)
OP_XOR(0, 4)
OP_XOR(1, 0)
OP_XOR(1, 1)
OP_XOR(1, 2)
OP_XOR(1, 3)
OP_XOR(1, 4)
OP_XOR(2, 0)
OP_XOR(2, 1)
OP_XOR(2, 2)
OP_XOR(2, 3)
OP_XOR(2, 4)
OP_XOR(3, 0)
OP_XOR(3, 1)
OP_XOR(3, 2)
OP_XOR(3, 3)
OP_XOR(3, 4)
OP_XOR(4, 0)
OP_XOR(4, 1)
OP_XOR(4, 2)
OP_XOR(4, 3)
OP_XOR(4, 4)

static opcode_func const opcodes[][5U][5U] = {
        [UVM_OP_NOP] =  {{&op_nop,&op_nop,&op_nop,&op_nop,&op_nop},
			 {&op_nop,&op_nop,&op_nop,&op_nop,&op_nop},
			 {&op_nop,&op_nop,&op_nop,&op_nop,&op_nop},
			 {&op_nop,&op_nop,&op_nop,&op_nop,&op_nop},
			 {&op_nop,&op_nop,&op_nop,&op_nop,&op_nop}
	},
        [UVM_OP_DEBUG] =  {{&op_debug,&op_debug,&op_debug,&op_debug,&op_debug},
			 {&op_debug,&op_debug,&op_debug,&op_debug,&op_debug},
			 {&op_debug,&op_debug,&op_debug,&op_debug,&op_debug},
			 {&op_debug,&op_debug,&op_debug,&op_debug,&op_debug},
			 {&op_debug,&op_debug,&op_debug,&op_debug,&op_debug}
	},
        [UVM_OP_RETURN] = {{&op_return,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error}
	},
        [UVM_OP_CALL] = {{&op_call,&op_call,&op_call,&op_call,&op_call},
			 {&op_call,&op_call,&op_call,&op_call,&op_call},
			 {&op_call,&op_call,&op_call,&op_call,&op_call},
			 {&op_call,&op_call,&op_call,&op_call,&op_call},
			 {&op_call,&op_call,&op_call,&op_call,&op_call}
	},
        [UVM_OP_LOAD] = {{&op_load_0, &op_load_1, &op_load_2, &op_load_3,
                          &op_load_4},
                         {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error}},
        [UVM_OP_LOADREF] = {{&op_loadref_0, &op_loadref_1, &op_loadref_2, &op_loadref_3,
                          &op_loadref_4},
                         {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error},
			 {&op_error,&op_error,&op_error,&op_error,&op_error}},
        [UVM_OP_MOVE] = {{&op_error, &op_move_0_1, &op_move_0_2,
                          &op_move_0_3, &op_move_0_4},
                         {&op_move_1_0, &op_error, &op_move_1_2,
                          &op_move_1_3, &op_move_1_4},
                         {&op_move_2_0, &op_move_2_1, &op_error,
                          &op_move_2_3, &op_move_2_4},
                         {&op_move_3_0, &op_move_3_1, &op_move_3_2,
                          &op_error, &op_move_3_4},
                         {&op_move_4_0, &op_move_4_1, &op_move_4_2,
                          &op_move_4_3, &op_error},

        },
        [UVM_OP_ADD] = {{&op_add_0_0, &op_add_0_1, &op_add_0_2, &op_add_0_3,
                            &op_add_0_4},
                           {&op_add_1_0, &op_add_1_1, &op_add_1_2, &op_add_1_3,
                            &op_add_1_4},
                           {&op_add_2_0, &op_add_2_1, &op_add_2_2, &op_add_2_3,
                            &op_add_2_4},
                           {&op_add_3_0, &op_add_3_1, &op_add_3_2, &op_add_3_3,
                            &op_add_3_4},
                           {&op_add_4_0, &op_add_4_1, &op_add_4_2, &op_add_4_3,
                            &op_add_4_4}

        },
        [UVM_OP_SUBTRACT] = {{&op_error, &op_sub_0_1, &op_sub_0_2,
                                 &op_sub_0_3, &op_sub_0_4},
                                {&op_sub_1_0, &op_error, &op_sub_1_2,
                                 &op_sub_1_3, &op_sub_1_4},
                                {&op_sub_2_0, &op_sub_2_1, &op_error,
                                 &op_sub_2_3, &op_sub_2_4},
                                {&op_sub_3_0, &op_sub_3_1, &op_sub_3_2,
                                 &op_error, &op_sub_3_4},
                                {&op_sub_4_0, &op_sub_4_1, &op_sub_4_2,
                                 &op_sub_4_3, &op_error},

        },
        [UVM_OP_XOR] = {{&op_xor_0_0, &op_xor_0_1, &op_xor_0_2,
                                 &op_xor_0_3, &op_xor_0_4},
                                {&op_xor_1_0, &op_xor_1_1, &op_xor_1_2,
                                 &op_xor_1_3, &op_xor_1_4},
                                {&op_xor_2_0, &op_xor_2_1, &op_xor_2_2,
                                 &op_xor_2_3, &op_xor_2_4},
                                {&op_xor_3_0, &op_xor_3_1, &op_xor_3_2,
                                 &op_xor_3_3, &op_xor_3_4},
                                {&op_xor_4_0, &op_xor_4_1, &op_xor_4_2,
                                 &op_xor_4_3, &op_xor_4_4},

        },
        [UVM_OP_GETQUAD] = {{&op_getquad_0_0, &op_getquad_0_1, &op_getquad_0_2,
                                 &op_getquad_0_3, &op_getquad_0_4},
                                {&op_getquad_1_0, &op_getquad_1_1, &op_getquad_1_2,
                                 &op_getquad_1_3, &op_getquad_1_4},
                                {&op_getquad_2_0, &op_getquad_2_1, &op_getquad_2_2,
                                 &op_getquad_2_3, &op_getquad_2_4},
                                {&op_getquad_3_0, &op_getquad_3_1, &op_getquad_3_2,
                                 &op_getquad_3_3, &op_getquad_3_4},
                                {&op_getquad_4_0, &op_getquad_4_1, &op_getquad_4_2,
                                 &op_getquad_4_3, &op_getquad_4_4},

        },
        [UVM_OP_SETQUAD] = {{&op_setquad_0_0, &op_setquad_0_1, &op_setquad_0_2,
                                 &op_setquad_0_3, &op_setquad_0_4},
                                {&op_setquad_1_0, &op_setquad_1_1, &op_setquad_1_2,
                                 &op_setquad_1_3, &op_setquad_1_4},
                                {&op_setquad_2_0, &op_setquad_2_1, &op_setquad_2_2,
                                 &op_setquad_2_3, &op_setquad_2_4},
                                {&op_setquad_3_0, &op_setquad_3_1, &op_setquad_3_2,
                                 &op_setquad_3_3, &op_setquad_3_4},
                                {&op_setquad_4_0, &op_setquad_4_1, &op_setquad_4_2,
                                 &op_setquad_4_3, &op_setquad_4_4},

        },

        [UVM_OP_RETCALL] = {{&op_retcall,&op_retcall,&op_retcall,&op_retcall,&op_retcall},
			 {&op_retcall,&op_retcall,&op_retcall,&op_retcall,&op_retcall},
			 {&op_retcall,&op_retcall,&op_retcall,&op_retcall,&op_retcall},
			 {&op_retcall,&op_retcall,&op_retcall,&op_retcall,&op_retcall},
			 {&op_retcall,&op_retcall,&op_retcall,&op_retcall,&op_retcall}
	},

        [UVM_OP_RETCALLIFNOTZERO]
        = {{&op_retcallifnotzero_0, &op_retcallifnotzero_1,
            &op_retcallifnotzero_2, &op_retcallifnotzero_3,
            &op_retcallifnotzero_4},
           {&op_error,&op_error,&op_error,&op_error,&op_error},
           {&op_error,&op_error,&op_error,&op_error,&op_error},
           {&op_error,&op_error,&op_error,&op_error,&op_error},
           {&op_error,&op_error,&op_error,&op_error,&op_error}},

        [UVM_OP_NEW] = {{&op_new_0_0, &op_new_0_1, &op_new_0_2,
                                 &op_new_0_3, &op_new_0_4},
                                {&op_new_1_0, &op_new_1_1, &op_new_1_2,
                                 &op_new_1_3, &op_new_1_4},
                                {&op_new_2_0, &op_new_2_1, &op_new_2_2,
                                 &op_new_2_3, &op_new_2_4},
                                {&op_new_3_0, &op_new_3_1, &op_new_3_2,
                                 &op_new_3_3, &op_new_3_4},
                                {&op_new_4_0, &op_new_4_1, &op_new_4_2,
                                 &op_new_4_3, &op_new_4_4},

        },

};

static inline opcode_func get_opcodes(uint8_t instr, uint8_t result_reg)
{
    /* 0 0
       1 01
       2 10
       3 11
       4 100
       5 101
       6 111
       7 1000
       15 1111
       255 1111 1111
     */
    uint8_t dest = result_reg & 7U;
    uint8_t src = (result_reg >> 3U) & 7U;
    instr = instr & (uint8_t)127U;

    // fprintf(stderr, "%s %u %u\n", instruction_name(instr), src, dest);

    return opcodes[instr][src % 5U][dest % 5U];
}

static uvm__value run(void const *base, uint16_t entry, uvm__value reg_0,
                      uvm__value reg_1, uvm__value reg_2, uvm__value reg_3,
                      uvm__value reg_4)
{
    struct context ctx = {0};
    ctx.base = base;
    ctx.instruction_pointer = ((uint8_t const *)base) + entry;
    ctx.stack_base = (uintptr_t)__builtin_frame_address(0);

    if (setjmp(ctx.jump_point)) {
        if (ctx.returning)
            return ctx.reg_0;
        uint8_t instruction = ctx.instruction_pointer[0U];
        uint8_t result_reg = ctx.instruction_pointer[1U];
        get_opcodes(instruction, result_reg)(&ctx, ctx.reg_0, ctx.reg_1,
                                             ctx.reg_2, ctx.reg_3, ctx.reg_4);
    } else {
        uint8_t instruction = ctx.instruction_pointer[0U];
        uint8_t result_reg = ctx.instruction_pointer[1U];
        get_opcodes(instruction, result_reg)(&ctx, reg_0, reg_1, reg_2, reg_3,
                                             reg_4);
    }
}

#if 0
static int byte_code_disassemble(FILE *file, char const *byte_code)
{
    size_t const size = code->byte_code_size;

    uint8_t const *const byte_code = code->byte_code;
    uint8_t const *const byte_code_end = byte_code + size;

    uint8_t const largest_register = code->largest_register;

    if (fprintf(file, "/* max register: r%" PRIu8 "*/ {\n", largest_register)
        < 0) {
        return -1;
    }

    uint8_t const *instruction_pointer = byte_code;
    while (instruction_pointer < byte_code_end) {
        uint8_t instruction = instruction_pointer[0U];

        if (instruction >= UVM_OP_FIRST_TERNARY
            && instruction <= UVM_OP_LAST_TERNARY) {
            uint8_t dest = instruction_pointer[1U] & 7U;
            uint8_t src = (instruction_pointer[1U] >> 3U);

            instruction_pointer += 2U;

            if (fprintf(file, "   %s r%" PRIu8 " r%" PRIu8 "\n",
                        instruction_name(instruction), dest, src)
                < 0) {
                return -1;
            }
            continue;
        }

        switch (instruction) {
        case UVM_OP_MOVE: {
            uint8_t dest = instruction_pointer[1U] & 7U;
            uint8_t src = (instruction_pointer[1U] >> 3U);

            instruction_pointer += 2U;

            if (fprintf(file, "   %s r%" PRIu8 " r%" PRIu8 "\n",
                        instruction_name(instruction), dest, src)
                < 0) {
                return -1;
            }
            break;
        }

        case UVM_OP_RETCALLIFNOTZERO:
        case UVM_OP_LOAD: {
            uint8_t result_reg = instruction_pointer[1U];
            uint8_t operand_a = instruction_pointer[2U];
            uint8_t operand_b = instruction_pointer[3U];

            instruction_pointer += 4U;

            if (fprintf(file, "   %s r%" PRIu8 " c%" PRIu16 "\n",
                        instruction_name(instruction), result_reg,
                        UINT8_TO_UINT16(operand_a, operand_b))
                < 0) {
                return -1;
            }
            break;
        }

        case UVM_OP_RETCALL:
        case UVM_OP_CALL: {
            uint8_t operand_a = instruction_pointer[1U];
            uint8_t operand_b = instruction_pointer[2U];

            instruction_pointer += 3U;

            if (fprintf(file, "   %s c%" PRIu16 "\n",
                        instruction_name(instruction),
                        UINT8_TO_UINT16(operand_a, operand_b))
                < 0) {
                return -1;
            }
            break;
        }

        case UVM_OP_RETURN:
        case UVM_OP_NOP:
            instruction_pointer += 1U;

            if (fprintf(file, "   %s\n", instruction_name(instruction)) < 0) {
                return -1;
            }
            break;

        default:
            errno = EINVAL;
            return -1;
        }
    }
    if (EOF == fputs("}\n", file)) {
        return -1;
    }

    return 0;
}
#endif

#define INSTRUCTION_NAMES(XX)                                                  \
    XX(UVM_OP_NOP, nop)                                                        \
    XX(UVM_OP_RETURN, return )                                                 \
    XX(UVM_OP_RETCALL, retcall)                                                \
    XX(UVM_OP_RETCALLIFNOTZERO, retcallifnotzero)                              \
    XX(UVM_OP_CALL, call)                                                      \
    XX(UVM_OP_LOAD, load)                                                      \
    XX(UVM_OP_LOADREF, loadref)                                                \
    XX(UVM_OP_MOVE, move)                                                      \
    XX(UVM_OP_ADD, add)                                                        \
    XX(UVM_OP_SUBTRACT, subtract)                                              \
    XX(UVM_OP_MULTIPLY, multiply)                                              \
    XX(UVM_OP_DIVIDE, divide)                                                  \
    XX(UVM_OP_AND, and)                                                        \
    XX(UVM_OP_OR, or)                                                          \
    XX(UVM_OP_XOR, xor)                                                        \
    XX(UVM_OP_NEW, new)                                                        \
    XX(UVM_OP_GETQUAD, getquad)                                                \
    XX(UVM_OP_SETQUAD, setquad)

struct instruction_names {
#define XX(X, Y) char Y##_field[sizeof #Y];
    INSTRUCTION_NAMES(XX)
#undef XX
};

static struct instruction_names const instruction_names = {
#define XX(X, Y) .Y##_field = #Y,
    INSTRUCTION_NAMES(XX)
#undef XX
};

static size_t const instruction_name_indices[] = {
#define XX(X, Y) [X] = offsetof(struct instruction_names, Y##_field),
    INSTRUCTION_NAMES(XX)
#undef XX
};

static char const *instruction_name(uint8_t index)
{
    return (char const *)&instruction_names + instruction_name_indices[index];
}
