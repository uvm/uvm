/*
 * Copyright 2014 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef UVM__BYTES_INCLUDE_H
#define UVM__BYTES_INCLUDE_H

#include <stdint.h>
#include <string.h>

/* This virtual machine is little endian */

/* Note: the copies seem redundant but they are needed for clang to
   optimize away the conversions to the identity on little endian
   machines. */

static inline uint16_t uvm__bytes_to_uint16(uint8_t const *const source)
{
    uint16_t network;
    memcpy(&network, source, sizeof network);

    uint8_t bytes[sizeof network];
    memcpy(bytes, &network, sizeof bytes);

    return ((uintmax_t)bytes[0U]) | (((uintmax_t)bytes[1U]) << 8U);
}

static inline int16_t uvm__bytes_to_int16(uint8_t const *const source)
{
    uint16_t network;
    memcpy(&network, source, sizeof network);

    uint8_t bytes[sizeof network];
    memcpy(bytes, &network, sizeof bytes);

    return ((intmax_t)bytes[0U]) | (((intmax_t)bytes[1U]) << 8U);
}

static inline uint32_t uvm__bytes_to_uint32(uint8_t const *const source)
{
    uint32_t network;
    memcpy(&network, source, sizeof network);

    uint8_t bytes[sizeof network];
    memcpy(bytes, &network, sizeof bytes);

    return ((uintmax_t)bytes[0U]) | (((uintmax_t)bytes[1U]) << 8U)
           | (((uintmax_t)bytes[2U]) << 16U) | (((uintmax_t)bytes[3U]) << 24U);
}

static inline uint64_t uvm__bytes_to_uint64(uint8_t const *const source)
{
    uint64_t network;
    memcpy(&network, source, sizeof network);

    uint8_t bytes[sizeof network];
    memcpy(bytes, &network, sizeof bytes);

    return ((uintmax_t)bytes[0U]) | (((uintmax_t)bytes[1U]) << 8U)
           | (((uintmax_t)bytes[2U]) << 16U) | (((uintmax_t)bytes[3U]) << 24U)
           | (((uintmax_t)bytes[4U]) << 32U) | (((uintmax_t)bytes[5U]) << 40U)
           | (((uintmax_t)bytes[6U]) << 48U) | (((uintmax_t)bytes[7U]) << 56U);
}
#endif /* UVM__BYTES_INCLUDE_H */
